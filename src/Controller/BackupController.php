<?php

namespace Drupal\allinone_backup\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;

/**
 * An allinone_backup controller.
 */
class BackupController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  protected $renderer;

  /**
   * Implements __construct().
   */
  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * Implements create().
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Implements content().
   */
  public function content() {
    $content['logfieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('backup log'),
      '#open' => TRUE,
    ];
    $content['logfieldset']['#markup'] = $this->renderer->render(views_embed_view('allinonebackup_block'));
    return $content;
  }

}
