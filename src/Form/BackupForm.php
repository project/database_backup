<?php

namespace Drupal\allinone_backup\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;

/**
 * Configure allinone_backup settings for this site.
 */
class BackupForm extends FormBase {

  /**
   * Escape data whene insert query run.
   */
  public function mysqlEscapeNoConn($input) {
    if (is_array($input)) {
      return array_map(__METHOD__, $input);
    }
    if (!empty($input) && is_string($input)) {
      return str_replace([
        '\\',
        "\0",
        "\n",
        "\r",
        "'",
        '"',
        "\x1a",
      ], [
        '\\\\',
        '\\0',
        '\\n',
        '\\r',
        "\\'",
        '\\"',
        '\\Z',
      ], $input);
    }
    return $input;
  }

  /**
   * Get Form ID.
   */
  public function getFormId() {
    return 'allinone_backup_form';
  }

  /**
   * Create form using buildForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $results = db_query("SHOW TABLES");
    $db = Database::getConnectionInfo();
    // Getting The List Of All Table for Exclude.
    while ($tablerow = $results->fetchObject()) {
      $tablename = 'Tables_in_' . $db['default']['database'];
      $tablelist[$tablerow->$tablename] = $tablerow->$tablename;
    }
    $form['backupfieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('Get backup now'),
      '#open' => TRUE,
    ];
    $form['backupfieldset']['exclude_tablelist'] = [
      '#title' => $this->t('Tables To Exclude'),
      '#type' => 'select',
      '#size' => 10,
      '#multiple' => TRUE,
      '#options' => $tablelist,
    ];
    $form['backupfieldset']['actions']['submit'] = [
      '#type' => 'submit',
      '#prefix' => '<br>',
      '#value' => $this->t('Backup Now'),
      '#button_type' => 'primary',
      '#weight' => '100',
    ];
    return $form;
  }

  /**
   * Form submit submitForm.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $limit = 200;
    $db = Database::getConnectionInfo();
    $new_folder = 'public://allinone/';
    if (!is_dir(drupal_realpath($new_folder))) {
      file_prepare_directory($new_folder, FILE_CREATE_DIRECTORY);
    }
    $excluded = $form_state->getValue('exclude_tablelist');
    // Get The excluded Tables List.
    $dbname = $db['default']['database'];
    // Get Current Database authenication for backup.
    $username = $db['default']['username'];
    $password = $db['default']['password'];
    $host = $db['default']['host'];
    $params = [
      'db_host' => $host,
      'db_uname' => $username,
      'db_password' => $password,
      'db_to_backup' => $dbname,
      'db_exclude_tables' => $excluded,
      // Put The Tables Name That We Won't To Backup.
    ];
    // IF exec not working on server.
    if (function_exists('exec')) {
      // ==========    IF exec working on server ========== //.
      $tabs = '';
      foreach ($excluded as $items) {
        $tabs .= ' --ignore-table=' . $dbname . '.' . $items;
      }
      $datestamp = date("Y-m-d-his");
      // Set The Backup File Name.
      $backup_file_name = $dbname . "-" . $datestamp . ".sql.gz";
      $command = "mysqldump " . $dbname . " $tabs -u" . $username . " -p" . $password . " --host " . $host . " | gzip > " . drupal_realpath($new_folder) . "/" . $backup_file_name;

      exec($command);

      // Change Permission to all user access.
      chmod(('sites/default/files/allinone/' . $backup_file_name), 0755);
      // Change Permission to all user access.
      chmod('sites/default/files/allinone', 0755);
      $sqlsize = filesize('sites/default/files/allinone/' . $backup_file_name);
      if ($sqlsize > 30) {
        drupal_set_message($this->t("Backup successfully"));
        // Add Content For Database Backup History Display.
        $node = Node::create([
          'type'        => 'backup',
          'title'       => $backup_file_name,
        ]);
        $node->save();
      }
    }
    else {
      $mtables = [];
      $contents = "-- Database: `" . $params['db_to_backup'] . "` --\n";
      $results = db_query("SHOW TABLES");
      // Getting The List Of All Table.
      while ($row = $results->fetchObject()) {
        $rowstable = (array) $row;
        $rowstable = array_values($rowstable);
        // Filter Tables = Total Tables - Exclude Table.
        if (!in_array($rowstable[0], $params['db_exclude_tables'])) {
          $mtables[] = $rowstable[0];
        }
      }

      // Loop The All Table Structure And Data.
      foreach ($mtables as $table) {
        $contents .= "\n\n-- Table Strucure For Table `" . $table . "` --\n\n";
        $contents .= "DROP TABLE IF EXISTS `" . $table . "`;\n\n";
        // Drop If Exists Table Query.
        $show_sql = "SHOW CREATE TABLE {$table}";
        $results = db_query($show_sql);
        // Create Table Query.
        while ($row = $results->fetchAssoc()) {
          $row = array_values($row);
          $contents .= str_replace('"', '`', $row[1]) . ";\n\n";
          // Created table query.
        }
        $table_sql = "SELECT * FROM {$table}";
        $results = db_query($table_sql);
        // Select Current Table Data For Insertion.
        $results->allowRowCount = TRUE;
        $row_count = $results->rowCount();
        $fields = $results->fetchAssoc();
        $fields = array_keys($fields);
        $fields_count = count($fields);
        // Insert Query Run If Records Is Available.
        if ($row_count > 0) {
          $insert_head = "INSERT INTO `" . $table . "` (";
          // Insert Data Query.
          for ($i = 0; $i < $fields_count; $i++) {
            $insert_head .= "`" . $fields[$i] . "`";
            if ($i < $fields_count - 1) {
              $insert_head .= ', ';
            }
          }
          $insert_head .= ")";
          $insert_head .= " VALUES\n";
          $r = 1;
          while ($row = $results->fetchAssoc()) {
            $row = array_values($row);

            if ($r == 1 ||($r % $limit) == 0) {
              $contents .= $insert_head;
            }
            $contents .= "(";

            for ($i = 0; $i < $fields_count; $i++) {
              $row_content = $row[$i];
              $contents .= "'" . $this->mysqlEscapeNoConn(trim($row_content)) . "'";
              // Escape Data.
              if ($i < $fields_count - 1) {
                $contents .= ', ';
              }
              else {
                $contents .= ' ';
              }
            }
            if (($r + 1) == $row_count || ($r % $limit) == $limit - 1) {
              $contents .= ");\n\n ";
            }
            else {
              $contents .= "),\n";
            }
            $r++;
          }
        }
      }
      $datestamp = date("Y-m-d-his");
      $backup_file_name_create = $dbname . "-" . $datestamp . ".sql";
      $fp = fopen(drupal_realpath($new_folder) . "/" . $backup_file_name_create, 'w+');

      if (fwrite($fp, $contents)) {
        chmod(('sites/default/files/allinone/' . $backup_file_name_create), 0777);
        // Change Permission to all user access.
        chmod('sites/default/files/allinone', 0777);
        drupal_set_message($this->t("Backup successfully"));
        // Add Content For Database Backup History Display.
        $node = Node::create([
          'type' => 'backup',
          'title' => $backup_file_name,
        ]);
        $node->save();
      }
      fclose($fp);
      $zip = new \ZipArchive();
      // Archive The Created File.
      $backup_file_name = $dbname . "-" . date('Ymd-His') . ".zip";
      $fp = drupal_realpath($new_folder) . "/" . $backup_file_name_create;

      if ($zip->open(drupal_realpath($new_folder) . '/' . $backup_file_name, \ZIPARCHIVE::CREATE) !== TRUE) {
        drupal_set_message($this->t("cannot open folder"));
      }
      $zip->addFromString($backup_file_name_create, file_get_contents($fp));
      $zip->close();
      unlink($fp);
    }
  }

}
